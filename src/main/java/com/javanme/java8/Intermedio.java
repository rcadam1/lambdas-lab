package com.javanme.java8;

import java.nio.file.Path;
import java.nio.file.Files;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Arrays;
import java.util.OptionalInt;
import  java.util.stream.Stream;
import  java.util.stream.Collectors;
import java.util.regex.Pattern;

/**
 * Clase con ejercicios nivel intermedio
 * Created by aalopez on 18/08/15.
 */
public class Intermedio {

    static final String REGEXP = "[- .:,]+"; // separa cadenas de texto en palabras

    /**
     * Contar el número de líneas no vacías que tiene el archivo pasado por parámetro.
     * Usar nuevos métodos encontrados en la clase java.nio.file.Files en Java 8 para obtener un Stream de 
     * las líneas de texto un archivo.
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Cantidad de líneas en el archivo
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     */
    public long ejercicio1(Path archivo) {
	try (Stream<String> lines = Files.lines(archivo))
	{
		return lines.filter(s -> s.length() > 0).count();
	} catch (IOException ex)
	{
		return -1;
	}
//        throw new UnsupportedOperationException();
    }

    /**
     * Encuentra el número de caracteres que tiene la línea más larga del archivo.
     * Usar nuevos métodos encontrados en la clase java.nio.file.Files en Java 8 para obtener un Stream de 
     * las líneas de texto de un archivo.
     * Para poder obtener un OptionalInt como resultado, debes convertir el Stream a uno primitivo. 
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Cantidad de caracteres que tiene la línea más larga del archivo
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     * @see java.util.stream.IntStream
     */
    public OptionalInt ejercicio2(Path archivo) {
	try (Stream<String> lines = Files.lines(archivo))
	{
		return lines.mapToInt(s -> {return s.length();} ).max();
	} catch (IOException ex)
	{
		return OptionalInt.of(-1);
	}
	
//        throw new UnsupportedOperationException();
    }

    /**
     * De las palabras que se encuentran en el archivo pasado por parámetro, conviertelas a minúsculas,
     * sin duplicados, ordenadas primero por tamaño y luego alfabeticamente.
     *
     * Une todas las palabras en una cadena de texto separando cada palabra por un espacio (" ")
     *
     * Usa la constante REGEXP proveida al inicio de esta clase para hacer la separación de cadenas de texto a palabras. 
     * Es posible que esta expresión retorne palabras vacías por lo que tendrás que adicIionar un filtro que las remueva.
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Cadena de texto que contiene las palabras en minúsculas, sin duplicados, ordenadas por tamaño, luego alfabeticamente
     * y cada palabra está separada por un espacio
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     * @see java.lang.String
     * @see java.util.stream.Collectors
     */
    public String ejercicio3(Path archivo) {
	try (Stream<String> lines = Files.lines(archivo))
	{
		Comparator<String> ord = (s1,s2) -> { int val,dif= s1.length() - s2.length(); val = (dif != 0) ? dif: s1.compareTo(s2); return val; };
//		return lines.map( (String s) -> Pattern.compile(" ").splitAsStream(s).collect(Collectors.joining(" " )) ).map(s -> s.toLowerCase() ).distinct().sorted(ord).collect(Collectors.joining(" "));
//		return lines.forEach( s -> Pattern.compile(" ").splitAsStream(s) ).flatMap(Stream::of).forEach(s -> s=s.toLowerCase()).distinct().sorted(ord).collect(Collectors.joining(" "));
		Stream<String> words = lines.flatMap(line -> Stream.of(line.split(REGEXP)));
		return words.filter(s -> s.length() > 0).map(s -> { return s.toLowerCase();}).distinct().sorted(ord).collect(Collectors.joining(" "));
	} catch (IOException ex)
	{
		return null;
	}
	
//        throw new UnsupportedOperationException();
    }

    /**
     * Categorizar TODAS las palabras de las primeras 10 líneas del archivo pasado por parámetro en un Map cuya llave es el
     * número de caracteres y el valor es el listado de palabras que tienen esa cantidad de caracteres
     *
     * Usa la constante REGEXP proveida al inicio de esta clase para hacer la separación de cadenas de texto a palabras. Es posible
     * que esta expresión retorne palabras vacías por lo que tendrás que adicionar un filtro que las remueva.
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Map cuya llave es la cantidad de caracteres y valor es el listado de palabras que tienen esa cantidad de
     * caracteres en las primeras 10 líneas del archivo
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     * @see java.lang.String
     * @see java.util.stream.Collectors
     */
    public Map<Integer, List<String>> ejercicio4(Path archivo) {
	try (Stream<String> lines = Files.lines(archivo))
	{
		Stream<String> words = lines.limit(10).flatMap(line -> Stream.of(line.split(REGEXP)));
		Map<Integer, List<String>> mapa = new HashMap<Integer, List<String>>();
		words.filter(w -> w.length() > 0).forEach( w -> {
			int length = w.length();
			List<String> l = mapa.get(length);
			if (l == null)
				mapa.put(length, l=new ArrayList<String>());
			l.add(w);
		});
		return mapa;
	}
	catch (IOException ex)
	{
		return null;
	}
//        throw new UnsupportedOperationException();
    }


    /**
     * Categorizar TODAS las palabras de las primeras 100 líneas del archivo pasado por parámetro en un Map cuya llave es la
     * palabra y el valor es la cantidad de veces que se repite la palabra
     * <p/>
     * Usa la constante REGEXP proveida al inicio de esta clase para hacer la separación de cadenas de texto a palabras. Es posible
     * que esta expresión retorne palabras vacías por lo que tendrás que adicionar un filtro que las remueva.
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Map cuya llave son las palabras de las primeras 100 líneas del archivo y su valor es la cantidad de veces que se repite
     * dicha palabra en las primeras 100 líneas del archivo
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     * @see java.lang.String
     * @see java.util.stream.Collectors
     */
    public Map<String, Long> ejercicio5(Path archivo) {
    	try (Stream<String> lines = Files.lines(archivo))
	{
		Stream<String> words = lines.limit(100).flatMap(line -> Stream.of(line.split(REGEXP)));
		Map<String,Long> mapa = new HashMap<String,Long>();
		words.filter(w -> w.length() > 0).forEach( w ->
			{
				int length = w.length();
				Long valor = mapa.get(w);
				if (valor == null)
					mapa.put(w,Long.valueOf(1));
				else
					mapa.put(w,Long.valueOf(valor + 1));
			});
		return mapa;
	}
	catch (IOException ex)
	{
		return null;
	}
//        throw new UnsupportedOperationException();
    }

    /**
     * Crear una doble agrupación de palabras únicas del archivo pasado por parámetro. Hacer la agrupación
     * en dos Maps. El Map externo tiene como llave la primera letra de la palabra en mayúsculas y como valor otro Map (el interno).
     * El Map interno debe tener como llave la cantidad de letras y como valor un listado de palabras que tienen esa cantidad
     * de letras.
     * <p/>
     * Por ejemplo, dadas las palabras "ermita sebastian sanisidro sancipriano cristorey chipichape"
     * El Map externo tendrá las llaves "E", "C", "S"
     * El valor para la llave "S" debe ser un Map con dos llaves: llave 9 y valor [sebastian sanisidro] (una lista de dos palabras)
     * y otra llave 11 con el valor [sancipriano] (una lista de un solo item)
     * <p/>
     * Usa la constante REGEXP proveida al inicio de esta clase para hacer la separación de cadenas de texto a palabras. Es posible
     * que esta expresión retorne palabras vacías por lo que tendrás que adicionar un filtro que las remueva.
     * Pista: Pasa las palabras a minúsculas para que el méotodo distinct las pueda filtrar correctamente
     *
     * @param archivo Ruta al archivo que se desea evaluar
     * @return Map cuya llave es la primera letra en mayúsculas de las palabras del archivo y su valor es otro Map cuya llave es la
     * cantidad de letras y su valor es el listado de palabras que tienen esa cantidad de letras
     * @see java.nio.file.Files
     * @see java.util.stream.Stream
     * @see java.lang.String
     * @see java.util.stream.Collectors
     */
    public Map<String, Map<Integer, List<String>>> ejercicio6(Path archivo) {
    	try (Stream<String> lines = Files.lines(archivo))
	{
		Stream<String> words = lines.flatMap(line -> Stream.of(line.split(REGEXP))).map(w -> w.toLowerCase());
		//Map<Integer,List<String> mi = new HashMap<Integer,List<String>>();
		Map<String, Map<Integer,List<String>>> me = new HashMap<String, Map<Integer,List<String>>>();

                //words.distinct().filter(w->w.length() > 7).forEach(System.out::println);

		words.distinct().filter(w -> w.length() > 0).forEach( w ->
			{
				String inicial = w.toUpperCase().substring(0,1);
				int length = w.length();
				Map<Integer,List<String>> mi = me.get(inicial);
				List<String> ls = null;
				if (mi == null)
				{
					mi = new HashMap<Integer,List<String>>();
					ls = new ArrayList<String>();
					ls.add(w);
					me.put(inicial,mi);
					mi.put(Integer.valueOf(length),ls);
				}
				else
				{
                                    	ls = mi.get(Integer.valueOf(length));
                                        if (ls == null)
                                        {
                                            ls = new ArrayList<String>();
                                            mi.put( Integer.valueOf(length),ls);
                                        }
                                        ls.add(w);
				}
                //System.out.println(me);
			});
		return me;
	}
	catch (IOException ex)
	{
		return null;
	}
//        throw new UnsupportedOperationException();
    }
}

